#include <stdio.h>
#include <math.h>

int main() {
    long n;
    scanf("%li", &n);

    for (long i = n; i <= 2*n; i++) {
        if (((i & (i - 1)) == 0)) {
            printf("%li\n", i);
            break;
        }
    }
    return 0;
}
