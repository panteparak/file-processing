//
// Created by Pan Teparak on 31/10/17.
//

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <ctype.h>

#define maxChar 17
#define maxText 500010

struct WordTally{
    int len, count;
};

FILE *openfile(char *fileLocation){
    FILE *file = fopen(fileLocation, "r");

    if (file == NULL){
        printf("file: %s is NULL\n", fileLocation);
        exit(1);
    }

    return file;
}


void strip(char *string, int n){
    for (int i = 0; i < n; ++i) if (isspace(string[i])) string[i] = '\0';
}

int sort(const struct WordTally * a, const struct WordTally  * b) {
    if (a->count == b->count){
        return b->len - a->len;
    }else{
        return b->count - a->count;
    }
}


int main(int argc, char *args[]){
    char buffer[maxChar];
    struct WordTally tally[maxChar];

    if (argc < 2){
        printf("Invalid Argument count\n");
        exit(0);
    }

    FILE *file = openfile(args[1]);


    for (int i = 0; i < maxChar; ++i) {
        tally[i].len = 0;
        tally[i].count = 0;
    }

    while (fgets(buffer, maxChar, file) != NULL) {
        strip(buffer, maxChar);
        int len = (int) strnlen(buffer, maxChar);
        if (len > 0){
            tally[len].count++;
            if (tally[len].len == 0){
                tally[len].len = len;
            }
        }
    }

    qsort(tally, maxChar, sizeof(struct WordTally), (int (*)(const void *, const void *)) sort);

    int rank = 0;
    for (int i = 0; i < maxChar; ++i) {
        if (tally[i].len != 0){
            printf("Rank %d: length %d sees %d word(s).\n", ++rank, tally[i].len, tally[i].count);
        }
    }
    return 0;

}
