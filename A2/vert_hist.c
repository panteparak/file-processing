#include <stdio.h>
#include <ctype.h>

#define alphasize 26
#define alphabet "abcdefghijklmnopqrstuvwxyz"

int getHighestPosition(int tally[], int n){
  int temp = 0;
  for (int i = 0; i < n; i++) if (tally[i] > temp) temp = tally[i];
  return temp;
}

void showTally(int tally[], int n){
  int highest = getHighestPosition(tally, alphasize);

  for (int i = highest-1; i >= 0; i--) {
    for (int j = 0; j < n; j++){
      if (i < tally[j]) printf("*");
      else printf(" ");
    }
    printf("\n");
  }
  printf("%s\n", alphabet);
}

int main(){
  int ascii;
  int tally[alphasize];
  for(int i = 0; i < alphasize; i++) tally[i] = 0;
  while ((ascii = getchar()) != EOF) {
    if (!isalpha(ascii)) continue;
    tally[tolower(ascii) - 97]++;
  }
  getHighestPosition(tally, alphasize);
  showTally(tally, alphasize);

  return 0;
}
