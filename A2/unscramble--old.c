//
// Created by Pan Teparak on 20/10/17.
//

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <ctype.h>

#define lenOfChar 21
#define lenOfDict 500000

//char dictionary[lenOfDict][lenOfChar];

struct Word {
    char word[lenOfChar];
    char sortedWord[lenOfChar];
};

struct Word dictionaryWord[lenOfDict];

char[] strip(char string[]){
//    string[strlen(string) - 1] = '\0';

    for (int i = 0; i < 21; ++i) {
        if (string[i] == '\n'){
            string[i] = '\0';
            break;
        }
    }

    return string;
}

int isWhitespace(char* string){
    if (strlen(string) == 1){
        return 1;
    }
    return 0;
}

int sortByWord(const struct Word * a, const struct Word  * b) {
    int r = strcasecmp(a->word, b->word);
    if (r) return r;
    return -strcmp(a->word, b->word);
}

int sortBySortedWord(const struct Word * a, const struct Word  * b){
    printf(a->sortedWord);
    int r = strcasecmp(a->sortedWord, b->sortedWord);
    if (r) return r;
    return -strcmp(a->sortedWord, b->sortedWord);
}

int sortByAlphabet(const void *a, const void *b) {
    int r = strcasecmp(a, b);
    if (r) return r;
    return -strcmp(a, b);
}

void readDictionary(char* fileLocation){
    FILE* file = fopen(fileLocation, "r");
    char buffer[lenOfChar];
    for (int i = 0; fgets(buffer, lenOfChar, file) ; i++) {
        buffer = strip(buffer);

        char t1[lenOfChar];
        char t2[lenOfChar];


        strncpy(t1, buffer, strlen(buffer));
        strncpy(t2, buffer, strlen(buffer));


        memcpy(dictionaryWord[i].word, t1, strlen(t1));
        memcpy(dictionaryWord[i].sortedWord, t2, strlen(t2));
        qsort(dictionaryWord[i].sortedWord, strlen(t2), sizeof(char), sortByAlphabet);


//        printf("%s %s\n", dictionaryWord[i].word, dictionaryWord[i].sortedWord);
    }
}

void readJumble(char* fileLocation){
    FILE* file = fopen(fileLocation, "r");
    char buffer[lenOfChar];

    for (int k = 0; k < 5; k++) {
        printf("%s %s\n", dictionaryWord[k].word, dictionaryWord[k].sortedWord);
    }


    for (int i = 0; fgets(buffer, lenOfChar, file) ; i++) {
        strip(buffer);
        if (isWhitespace(buffer)){
            continue;
        }
        char temp[lenOfChar];

        strncmp(temp, buffer, strlen(buffer));

        qsort(temp, strlen(temp), sizeof(char), sortByAlphabet);
//        printf("%s\n", temp);

        printf("%s:", buffer);

        int found = 0;
        for (int j = 0; j < lenOfDict; j++) {
            if (strcmp(dictionaryWord[j].sortedWord, temp) == 0){
                found = 1;

                printf(" %s", dictionaryWord[j].word);
            }

        }

        if (found){
            printf("\n");
        }else{
            printf(" NO MATCHES\n");
        }
    }
}



int main(int argc, char * args[]){

    char* dictArgs = args[1];
    char* jumbleArgs = args[2];

    readDictionary(dictArgs);

    int size;
    for (size = 0; size < lenOfDict; size++){
//        printf("%s\n", dictionaryWord[size].word);
    }

//    qsort(&dictionaryWord, (size_t) size, sizeof(char) * lenOfChar, (int (*)(const void *, const void *)) sortByWord);

    readJumble(jumbleArgs);
}