#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

void replaceWhitespaceWithTab(int n) {
//    d
    int ch, count = 0;
    while ((ch = getchar()) != EOF){
        if (ch != ' '){
            if (count != 0){
                for (int i = 0; i < count; i++) {
                    printf("%c", ' ');
                }
                count = 0;
            }
            printf("%c", ch);
        }
        else {
            if (ch == ' ') {
                count++;
                if (count == n) {
                    printf("%c",'\t');
                    count = 0;
                }
            } else {
                for (int i = 0; i < count; i++) {
                    printf("%c", ' ');
                }

                printf("%c", ch);
                count = 0;
            }
        }
    }
    printf("%c", '\n');
}

void replaceTabWithWhitespace(int n){
//    e
    int ch;
    while((ch = getchar()) != EOF){
        if(ch == '\t')
            for (int i = 0; i < n; i++)
                printf("%c", ' ');
        else
            printf("%c", ch);
    }

    printf("%c", '\n');

}

int main(int argc, char* argv[]) {
    if (argc <= 1) {
        printf("%s\n", "No Argument supplied");
        return 0;
    }

    if (argv[1] == NULL || argv[2] == NULL) {
        printf("%s\n", "Invalid Argument");
        return 0;
    }
    int n = atoi(argv[2]);

    if (strcmp(argv[1], "-e") == 0) {
        replaceWhitespaceWithTab(n);
    }

    if (strcmp(argv[1], "-d") == 0) {
        replaceTabWithWhitespace(n);
    }
    return 0;
}
