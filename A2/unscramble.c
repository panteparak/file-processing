#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <ctype.h>

#define maxDict 500000
#define maxChar 35

struct Word {
    char word[maxChar];
    char sortedWord[maxChar];
};

void strip(char *string, int n){
    for (int i = 0; i < n; ++i) if (isspace(string[i])) string[i] = '\0';
}

int sortByWord(const struct Word * a, const struct Word  * b) {
    int r = strcasecmp(a->word, b->word);
    if (r) return r;
    return -strcmp(a->word, b->word);
}

int sortBySortedWord(const struct Word * a, const struct Word  * b){
//    printf(a->sortedWord);
    int r = strcasecmp(a->sortedWord, b->sortedWord);
    if (r) return r;
    return -strcmp(a->sortedWord, b->sortedWord);
}

int sortByAlphabet(const void *a, const void *b) {
    int r = strcasecmp(a, b);
    if (r) return r;
    return -strcmp(a, b);
}

FILE *openfile(char *fileLocation){
    FILE *file = fopen(fileLocation, "r");

    if (file == NULL){
        printf("file: %s is NULL\n", fileLocation);
        exit(1);
    }

    return file;
}

struct Word** readDictionary(char *fileLocation){

    FILE *file = openfile(fileLocation);


    char ch[maxChar];
    struct Word ** dictionary = (struct Word**)malloc(sizeof(struct Word*) * maxDict);

    for (int i = 0; fgets(ch, maxChar, file) != NULL; i++) {
        strip(ch, maxChar);

        char t1[maxChar];
        char t2[maxChar];

        strncpy(t1, ch, maxChar);
        strncpy(t2, ch, maxChar);
        qsort(t2, strlen(t2), sizeof(char), sortByAlphabet);


        strncpy((*dictionary[i]).word, t1, strlen(t1));
        strncpy((*dictionary[i]).sortedWord, t2, strlen(t2));
    }

    return dictionary;
}

void readJumble(char *fileLocation, struct Word** dictionary){
    FILE *file = openfile(fileLocation);

    char ch[maxChar];
    for (int i = 0; fgets(ch, maxChar, file); i++) {
        strip(ch, maxChar);

        printf("%s:", ch);
        qsort(ch, strlen(ch), sizeof(char), sortByAlphabet);

        int found = 0;
        for (int j = 0; j < maxDict && dictionary[j] != NULL; j++) {
            if (strcmp(ch, dictionary[j]->sortedWord) == 0){
                found++;
                printf(" %s", dictionary[j]->word);
            }
        }

        if (found)
            printf("\n");
        else
            printf(" NO MATCHES\n");
    }
}

int main(int argc, char *args[]){

    if (argc <= 1){
        printf("%s", "Invalid argument count");
        exit(0);
    }

    char *dictArgs = args[1];
    char *jumbleArgs = args[2];

    struct Word** dictionary = readDictionary(dictArgs);

    readJumble(jumbleArgs, dictionary);

    for (int i = 0; i < maxDict; i++) {
        free(dictionary[i]);
        dictionary[i] = NULL;
    }

    free(dictionary);
    dictionary = NULL;

    return 0;
}
