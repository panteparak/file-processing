//
// Created by Pan Teparak on 21/10/17.
//

#include <printf.h>
#include <stdlib.h>
#include <memory.h>

void init(int ***num){
    *num = (int**)malloc(sizeof(int*));

    **num = (int*)malloc(sizeof(int));

//    int n = 10;
    ***num = 10;
//    memcpy(***num, n, sizeof(int));
}

struct Book {
    char *ISBN;
    int *count;
};

void init2(struct Book *book){
    book = (struct Book*)malloc(sizeof(struct Book));
    book->ISBN = (char*)malloc(sizeof(char) * 20);
    book->count = (int)malloc(sizeof(int));

    strncpy(*(book->ISBN), "ABCDEFG", 20);
    *(book->count) = 10;

}


int main(){
    int **n;
    struct Book *book = NULL;

    init(&n);
    init2(book);

    printf("%d\n", **n);
    printf("%s %d\n", book->ISBN, book->count);
    return 0;
}
