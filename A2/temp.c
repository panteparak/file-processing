//
// Created by Pan Teparak on 24/10/17.
//


#include <stdlib.h>
#include <printf.h>

int main(){
    int d[] = {1,2};
    int e[] = {3, 4};
    int **p = (int **)malloc(sizeof(int*) * 4);

    p[0] = &d[0];
    p[1] = &d[1];
    p[2] = &e[0];
    p[3] = &e[1];

    printf("%d %d %d %d\n", p[0][0], p[0][1], p[1][0], p[1][1]);
}
