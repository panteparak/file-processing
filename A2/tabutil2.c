#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(int argc, char *argv[]) {
    int c;
    int j;
    int count;
    int val;
    char str[20];
    strcpy(str, argv[2]);
    val = atoi(str);
    if (argc == 3) {
        if (!strcmp(argv[1], "-d")) {
            while ((c = getchar()) != EOF) {
                if (c == '\t')
                    for (j = 0; j < val; j++) {
                        putchar(' ');
                    }
                else putchar(c);
            }
        } else {
            while ((c = getchar()) != EOF) {
                if (c == ' ') {
                    count++;
                    if (count == val) {
                        putchar('\t');
                        count = 0;
                    }
                } else {
                    for (int i = 0; i < count; i++) {
                        putchar(' ');
                    }
                    putchar(c);
                    count = 0;
                }
            }
        }
    }
}
