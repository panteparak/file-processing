/* Name: Pan Teparak
 * ID: 5780665
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "mergesort.h"

void strip(char *string, int n){
    for (int i = 0; i < n; ++i)
        if (isspace(string[i]))
            string[i] = '\0';
}

void merge(Entry *output, Entry *L, int nL, Entry *R, int nR) {
    int i = 0, j = 0, k = 0;
    for (; k < (nL + nR) ; k++) {
        if ((j >= nR) || (i < nL && L[i].data < R[j].data))
            output[k] = L[i++];
        else
            output[k] = R[j++];
    }
}

void merge_sort(Entry *entries, int n) {
    if (n > 1){
        int mid = n / 2;
        Entry *temp = (Entry*) calloc(n, sizeof(Entry));
        Entry* left = entries;
        Entry* right = &(entries[mid]);

        int nL = mid;
        int nR = n - mid;
        merge_sort(left, nL);
        merge_sort(right, nR);

        merge(temp, left, nL, right, nR);

        for (int i = 0; i < n; ++i) {
            entries[i] = temp[i];
        }

        free(temp);
    }
}

void print(Entry *entry, int n){
    for (int i = 0; i < n; ++i)
        printf("%d %s\n", entry[i].data, entry[i].name);
}



/*
TEST: ./mergesort < test.in
OUTPUT:
1 lsdfjl
2 ghijk
3 ksdljf
5 abc
6 def
*/
int main(void) {
    int n;
    Entry *array;
    scanf("%d", &n);

    array = calloc(n, sizeof(Entry));
    for (int i = 0; i < n; ++i) {
        int data;
        array[i].name = (char *)calloc(MAX_NAME_LENGTH, sizeof(char));
        scanf("%d %s", &data, array[i].name);
        strip(array[i].name, MAX_NAME_LENGTH);
        array[i].data = data;
    }

    merge_sort(array, n);
    print(array, n);

    for (int i = 0; i < n; ++i) {
        free(array[i].name);
    }

    free(array);

}

