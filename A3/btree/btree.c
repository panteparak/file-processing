/* Name: Pan Teparak
 * ID: 5780665
 */
#include <stdlib.h>
#include <stdio.h>
#include "btree.h"

int isEmpty(Node *node){
    return node == NULL;
}

void insert(Node **tree, int val) {
    if (isEmpty(*tree)){
        (*tree) = (Node *)malloc(sizeof(Node));
        (*tree)->data = val;
        (*tree)->left = NULL;
        (*tree)->right = NULL;
    } else {
        if (val > (*tree)->data) {
            insert(&(*tree)->right, val);
        } else {
            insert(&(*tree)->left, val);
        }
    }
}

void print_helper(Node *tree, int lv){
    if (tree != NULL){
        printf("%*s|- %d\n", 2*lv, "", tree->data);
        print_helper(tree->left, lv + 1);
        print_helper(tree->right, lv + 1);
    }
}

void print(Node *tree) {
    printf("%d\n", tree->data);
    print_helper(tree->left, 0);
    print_helper(tree->right, 0);
}

void delete(Node *tree) {
    if (tree != NULL){
        if (tree->left != NULL)
            delete(tree->left);
        if (tree->right != NULL)
            delete(tree->right);

        free(tree);
    }
}

Node *lookup(Node ** tree, int val) {
    if (*tree == NULL)
        return NULL;

    if ((*tree)->data == val)
        return (*tree);
    else if (val > (*tree)->data)
        return lookup(&(*tree)->right, val);
    else
        return lookup(&(*tree)->left, val);
}


/***** Expected output: *****
7
|-2
  |-1
  |-4
|-10
  |-15
    |-12
      |-11
Found
Not Found
 *****************************/
int main(int argc, char **argv)
{
    Node *root = NULL;
    Node *target = NULL;

    // add nodes
    insert(&root, 7);
    insert(&root, 2);
    insert(&root, 4);
    insert(&root, 10);
    insert(&root, 1);
    insert(&root, 15);
    insert(&root, 12);
    insert(&root, 11);
    insert(&root, 8); //Added

    // Lets see how the tree looks
    print(root);

    // Check if 4 in the tree
    target = lookup(&root, 2);
    if (target) {
        printf("Found\n");
    } else {
        printf("Not Found\n");
    }

    // Check if 44 in the tree
    target = lookup(&root, 44);
    if (target) {
        printf("Found\n");
    } else {
        printf("Not Found\n");
    }

    // Now let's free up the space
    delete(root);
}
