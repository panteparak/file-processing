#include <stdio.h>
#include <stdlib.h>

void dealloc(int ***a, int n){
    for (int i = 0; i < n; i++)
        free((*a)[i]);
    free(*a);
}

double compute_det(int **a, int n){
    if (n == 1)
        return a[0][0];
    if (n == 2)
        return a[0][0] * a[1][1] - a[1][0] * a[0][1];

    if (n == 3){
        int A = a[0][0], B = a[0][1], C = a[0][2], D = a[1][0], E = a[1][1], F = a[1][2], G = a[2][0], H = a[2][1], I = a[2][2];
        return A*(E*I - F*H) - B*(D*I - F*G) + C*(D*H - E*G);
    }

    int **temp;
    double determinant = 0;

    for (int k = 0; k < n; k++) {
        temp = malloc(sizeof(int *) * (n - 1));
        for (int i = 0; i < n - 1; i++)
            temp[i] = malloc(sizeof(int) * (n - 1));

        for (int i = 1; i < n; i++) {
            for (int j = 0, l = 0; j < n; j++) {

                if (j != k){
                    temp[i - 1][l++] = a[i][j];
                }
            }
        }

        determinant += (k % 2 == 0 ? 1 : -1) * a[0][k] * compute_det(temp, n-1);
        dealloc(&temp, n - 1);
    }

    return determinant;
}

/*
TEST: ./det < det.in
OUTPUT:
-105.00000
*/
int main(void) {
    // implement this
    int n;
    scanf("%d", &n);

    int **array = (int**)malloc(sizeof(int*) * n);

    for (int i = 0; i < n; i++) {
        array[i] = (int *)malloc(sizeof(int) * n);
        for (int j = 0; j < n; j++){
            int t;
            scanf("%d", &t);
            array[i][j] = t;
        }
    }

    double ans = compute_det(array, n);

    printf("%.5f\n", ans);


    dealloc(&array, n);
}
