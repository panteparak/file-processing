/* Name: Pan Teparak
 * ID: 5780665
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "queue.h"

void push(Queue **q, char *word) {
//    struct node_struct *temp = malloc(sizeof(struct node_struct));
//    temp->data = (char *)malloc(sizeof(char) * strlen(word) + 1);
//    strncpy(temp->data, word, strlen(word));

//    temp->next = NULL;

//    if (isEmpty(*q)){
//        if (*q == NULL)
//            (*q) = (Queue *)malloc(sizeof(Queue));
//
//        (*q)->head = malloc(sizeof(struct node_struct));
//        (*q)->head->data = (char *)malloc(sizeof(char) * strlen(word));
//        strncpy((*q)->head->data, word, strlen(word));
//        (*q)->head->next = NULL;
//        (*q)->tail = (*q)->head;
//    } else {
//        (*q)->tail->next = malloc(sizeof(struct node_struct));
//        (*q)->tail->next->data = (char *)malloc(sizeof(char) * strlen(word));
//
//        strncpy((*q)->tail->next->data, word, strlen(word));
//
//        (*q)->tail = (*q)->tail->next;
//        (*q)->tail->next = NULL;
//    }

    struct node_struct *temp = malloc(sizeof(struct node_struct));
    temp->next = NULL;
    temp->data = (char *)malloc(sizeof(char) * strlen(word) + 1);
    strcpy(temp->data, word);


    if (isEmpty(*q)){
        if (*q == NULL) *q = (Queue *)malloc(sizeof(Queue));
        (*q)->head = temp;
        (*q)->tail = temp;

    }else {
        (*q)->tail->next = temp;
        (*q)->tail = temp;
    }
}

char *pop(Queue *q) {
//    if (isEmpty(q))
//        return NULL;
//
//    struct node_struct *node = q->head;
//    struct node_struct *next = node->next;
//    if (node == NULL){
//        q->head = NULL;
//        q->tail = NULL;
//    } else
//        q->head = next;
//    char *data = node->data;
//
//    free(node);
//    return data;

    if (isEmpty(q))
        return NULL;

    struct node_struct *node = q->head;

    char *out = node->data;
    if (node->next == NULL){
        q->head = NULL;
        q->tail = NULL;

    } else {
        q->head = q->head->next;
    }

    free(node);
    return out;
}

void print(Queue *q) {
    if (isEmpty(q)){
        printf("No items\n");
    } else{
        struct node_struct *head = q->head;
        while (head != NULL){
            printf("%s\n", head->data);
            head = head->next;
        }
    }

}

int isEmpty(Queue *q) {
    return q == NULL || (q->head == NULL && q->tail == NULL);
}

void delete(Queue *q) {
//    if (! isEmpty(q)){
//        struct node_struct *node = q->head;
//        while (node != NULL){
//            free(node->data);
//            free(node);
//            node = node->next;
//        }
//
//        q->head = NULL;
//        q->tail = NULL;
//    }

    while (! isEmpty(q)){
        char * a = pop(q);
        free(a);
    }
}

/***** Expected output: *****
No items
a
b
c
a
b
c
d
e
f
No items
s = World
t = Hello
*****************************/
//int main(int argc, char **argv)
//{
//    Queue *q = NULL;
//
//    // print the queue
//    print(q);
//
//    // push items
//    push(&q, "a");
//    push(&q, "b");
//    push(&q, "c");
//    print(q);
//
//    // pop items
//    while (!isEmpty(q)) {
//        char *item = pop(q);
//        printf("%s\n", item);
//        free(item);
//    }
//
//    char *item = pop(q);
//    assert(item == NULL);
//
//    // push again
//    push(&q, "d");
//    push(&q, "e");
//    push(&q, "f");
//    print(q);
//
//    // destroy the queue
//    delete(q);
//
//    // print again
//    print(q);
//
//    // check copy
//    char *s = (char *)malloc(10);
//    strcpy(s, "Hello");
//    push(&q, s);
//    strcpy(s, "World");
//    char *t = pop(q);
//    printf("s = %s\n", s);
//    printf("t = %s\n", t);
//    free(t);
//    free(s);
//
//    // free the queue
////    free(q);
//
//    print(q);
//    push(&q, "d");
//    push(&q, "e");
//    push(&q, "f");
//    print(q);
//    char *a =  pop(q);
//    char *b = pop(q);
//    char *c = pop(q);
//
//    free(a);
//    free(b);
//    free(c);
//
//    print(q);
//    free(q);
//}

int main(int argc, char **argv)
{
    Queue *q = NULL;

    // print the queue
    print(q);

    // push items
    push(&q, "a");
    push(&q, "b");
    push(&q, "c");
    print(q);

    // pop items
    while (!isEmpty(q)) {
        char *item = pop(q);
        printf("%s\n", item);
        free(item);
    }

    char *item = pop(q);
    assert(item == NULL);
    // printf("OVERHERE\n");

    // push again
    push(&q, "d");
    push(&q, "e");
    push(&q, "f");
    // printf("PASSED PUSH\n");
    print(q);

    // destroy the queue
    delete(q);

    // print again
    print(q);

    // check copy
    char *s = (char *)malloc(10);
    strcpy(s, "Hello");
    push(&q, s);
    strcpy(s, "World");
    char *t = pop(q);
    printf("s = %s\n", s);
    printf("t = %s\n", t);
    free(t);
    free(s);

    // free the queue
     free(q);
//    print(q);
//    push(&q, "d");
//    push(&q, "e");
//    push(&q, "f");
//    char *a =  pop(q);
//    char *b = pop(q);
//    char *c = pop(q);
//
//    free(a);
//    free(b);
//    free(c);
//
//    print(q);
//    free(q);
}
