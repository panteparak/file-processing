# File Processing T1 2017 #

### This is a homework git repository, feel free to peek or ask questions personally but plagiarism is discourages unless you're courages enough. ###

 ---
### Assignments ###
|#      |Assignments        |Due            | Status        | Branch |
| :---: |:----------------:|:------------:|:-------------:|:-------------:|
| A1    | Shell Scripting  | 06 - OCT - 17 | Completed     | master
| A2    | C Programming    | 20 - OCT - 17 | Completed     | master
| A3    | Dynamic Memory   | 03 - NOV - 17 | Completed   | master
| A4    | Bit Lab   | 19 - NOV - 17 | In-Completed   | A4

### Exam ###
|#      |Assignments        |Due            | Status        | Branch |
| :---: |:----------------:|:------------:|:-------------:|:-------------:|
| Midterm    | C Programming   | 31 - Oct - 17 | Completed   | master


#### Found a bug? Look for me in 1409
